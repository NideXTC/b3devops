FROM node:latest
WORKDIR /src
COPY ./package.json .
COPY ./email.js .
COPY ./email.spec.js .
RUN npm install
CMD ["npm","test"]
EXPOSE 3000