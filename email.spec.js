const email = require('./email');

// https://facebook.github.io/jest/docs/en/using-matchers.html
describe('Test email', () => {
  it('should contain an @', () => {
    return expect(email('toto@at.fr')).toBe(true);
  });

  it('should contain only one @', () => {
    return expect(email('toto@@at.fr')).toBe(false);
  });

  it('should have text before @', () => {
    return expect(email('@at.fr')).toBe(false);
  });

  it('should not contain special chars', () => {
    return expect(email('fezij!fezkjnv@toto.fr')).toBe(false);
  });

  it('should have at least 2 chars after the last dot', () => {
    return expect(email('fezijfezkjnv@toto.f')).toBe(false);
  });

  it('should contain dots in text', () => {
    return expect(email('fezijfez.kjnv@toto.fr')).toBe(true);
  });
});